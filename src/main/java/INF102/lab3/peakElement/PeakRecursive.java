package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        return findPeakRecursivly(numbers, 0, numbers.size() - 1);
    }

    private int findPeakRecursivly(List<Integer> list, int lower, int upper) {
        int middle = (lower + upper) / 2;

        if (lower == upper) return list.get(lower);
        if (list.get(middle) < list.get(middle + 1)) return findPeakRecursivly(list, middle + 1, upper);
        else return findPeakRecursivly(list, lower, middle);
    }

}
