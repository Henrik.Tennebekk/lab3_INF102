package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int lower = number.getLowerbound();
        int upper = number.getUpperbound();

        while (true) {
            int middle = (lower + upper) / 2;
            int guess = number.guess(middle);

            if (guess == 1) {
                upper = middle; 
            } else if (guess == -1) {
                lower = middle;
            } else {
                return middle;
            }
        }
    }

}
